import { QuadrilateralFactory } from '../factories/QuadrilateralFactory';
import { CubeFactory } from '../factories/CubeFactory';
import { PointFactory } from '../factories/PointFactory';
import { DataValidator } from '../validators/DataValidator';
import { logger } from './Logger';
import { Cube } from '../models/Cube';
import { Quadrilateral } from "../models/Quadrilateral";

export function createShape(lines: string[]): (Quadrilateral | Cube)[] {
    const shapes: (Quadrilateral | Cube)[] = [];

    lines.forEach((line, index) => {
        const parts = line.trim().split(' ');
        const type = parts.shift();

        if (type === 'Q' && DataValidator.isValidPointData(parts)) {
            const [x1, y1, x2, y2, x3, y3, x4, y4] = parts.map(Number);
            const p1 = PointFactory.create2DPoint(x1, y1);
            const p2 = PointFactory.create2DPoint(x2, y2);
            const p3 = PointFactory.create2DPoint(x3, y3);
            const p4 = PointFactory.create2DPoint(x4, y4);
            try {
                const quadrilateral = QuadrilateralFactory.createQuadrilateral(`quad-${index}`, p1, p2, p3, p4);
                shapes.push(quadrilateral);
                logger.info(`${quadrilateral.id} created successfully from line ${index + 1}`);
            } catch (error) {
                logger.error(`Error creating quadrilateral from line ${index + 1}: ${error instanceof Error ? error.message : 'Unknown error'}`);
            }
        } else if (type === 'C' && DataValidator.isValidCubeData(parts)) {
            const [x, y, z, edgeLength] = parts.map(Number);
            const p1 = PointFactory.create3DPoint(x, y, z);
            try {
                const cube = CubeFactory.createCube(p1, edgeLength, `cube-${index}`);
                shapes.push(cube);
                logger.info(`${cube.getId()} created with edge length ${edgeLength} at origin (${x}, ${y}, ${z})`);
            } catch (error) {
                logger.error(`Error creating cube from line ${index + 1}: ${error instanceof Error ? error.message : 'Unknown error'}`);
            }
        } else {
            logger.error(`Line ${index + 1}: Invalid data for shape creation.`);
        }
    });

    return shapes;
}
