import { Quadrilateral } from '../models/Quadrilateral';
import { Point } from '../models/Point';

export class QuadrilateralFactory {
    static createQuadrilateral(name: string, p1: Point, p2: Point, p3: Point, p4: Point): Quadrilateral {
        if (Quadrilateral.collinear(p1, p2, p3) || Quadrilateral.collinear(p2, p3, p4) || Quadrilateral.collinear(p3, p4, p1)) {
            throw new Error('Three points are collinear. Not a valid quadrilateral.');
        }
        return new Quadrilateral(name, p1, p2, p3, p4);
    }
}
