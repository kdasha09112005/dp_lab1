import path from 'path';
import { logger } from './utils/Logger';
import { readShapesFromFile } from './utils/ShapeReader';
import { createShape } from './utils/ShapeCreator';
import { Cube } from './models/Cube';

try {
    const filePath = path.resolve("dist", '../data/shapes.txt');
    const lines = readShapesFromFile(filePath);
    const shapes = createShape(lines);

    logger.info(`Total shapes processed: ${shapes.length}`);
    shapes.forEach(shape => {
        if (shape instanceof Cube) {
            console.log(`${shape.getId()} - Surface Area: ${shape.surfaceArea()}, Volume: ${shape.volume()}, Volume Ratio after slicing XY: ${shape.volumeRatioAfterSliceXY()}, Is Base On Coordinate Plane: ${shape.baseOnCoordinatePlane()}`);
        } else if ('area' in shape) {
            console.log(`${shape.id} - Area: ${shape.area()}, Perimeter: ${shape.perimeter()}, Is Rectangle: ${shape.isRectangle()}, Is Square: ${shape.isSquare()}, Is Convex: ${shape.isConvex()}, Is Rhombus: ${shape.isRhombus()}, Is Trapezoid: ${shape.isTrapezoid()}`);
        }
    });
} catch (error) {
    console.log(`Unhandled exception in reading shapes: ${error instanceof Error ? error.message : 'Unknown error'}`);
}
