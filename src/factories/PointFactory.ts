import { Point } from '../models/Point';
import { DataValidator } from '../validators/DataValidator';

const POINT_ERROR: string = "Coordinates must be a number";

export class PointFactory {
    static create2DPoint(x: number, y: number): Point {
        if (DataValidator.isEmpty(x) || DataValidator.isEmpty(y)) {
            throw new Error(`${POINT_ERROR}`);
        } 
        return new Point(x, y);
    }

    static create3DPoint(x: number, y: number, z: number): Point {
        if (DataValidator.isEmpty(x) || DataValidator.isEmpty(y) || DataValidator.isEmpty(z)) {
            throw new Error(`${POINT_ERROR}`);
        } 
        return new Point(x, y, z);
    }
}
