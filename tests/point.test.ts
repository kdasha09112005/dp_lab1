import { PointFactory } from '../src/factories/PointFactory';
import { Point } from '../src/models/Point';
import { describe, test, expect } from "@jest/globals";

describe('PointFactory for 2D Points', () => {
    test('should create a valid 2D point', () => {
        const point = PointFactory.create2DPoint(1, 2);
        expect(point).toBeInstanceOf(Point);
        expect(point.x).toBe(1);
        expect(point.y).toBe(2);
        expect(point.z).toBe(0);
    });
});

describe('PointFactory for 3D Points', () => {
    test('should create a valid 3D point', () => {
        const point = PointFactory.create3DPoint(1, 2, 3);
        expect(point).toBeInstanceOf(Point);
        expect(point.x).toBe(1);
        expect(point.y).toBe(2);
        expect(point.z).toBe(3);
    });
});

describe('PointFactory with invalid data', () => {
    test('should throw an error if x is invalid for 2D point', () => {
        expect(() => PointFactory.create2DPoint(null as any, 2)).toThrow();
        expect(() => PointFactory.create2DPoint(undefined as any, 2)).toThrow();
        expect(() => PointFactory.create2DPoint(NaN, 2)).toThrow();
    });

    test('should throw an error if y is invalid for 2D point', () => {
        expect(() => PointFactory.create2DPoint(1, null as any)).toThrow();
        expect(() => PointFactory.create2DPoint(1, undefined as any)).toThrow();
        expect(() => PointFactory.create2DPoint(1, NaN)).toThrow();
    });

    test('should throw an error if x is invalid for 3D point', () => {
        expect(() => PointFactory.create3DPoint(null as any, 2, 3)).toThrow();
        expect(() => PointFactory.create3DPoint(undefined as any, 2, 3)).toThrow();
        expect(() => PointFactory.create3DPoint(NaN, 2, 3)).toThrow();
    });

    test('should throw an error if y is invalid for 3D point', () => {
        expect(() => PointFactory.create3DPoint(1, null as any, 3)).toThrow();
        expect(() => PointFactory.create3DPoint(1, undefined as any, 3)).toThrow();
        expect(() => PointFactory.create3DPoint(1, NaN, 3)).toThrow();
    });

    test('should throw an error if z is invalid for 3D point', () => {
        expect(() => PointFactory.create3DPoint(1, 2, null as any)).toThrow();
        expect(() => PointFactory.create3DPoint(1, 2, undefined as any)).toThrow();
        expect(() => PointFactory.create3DPoint(1, 2, NaN)).toThrow();
    });

    test('should not throw an error if coordinates are negative for 2D point', () => {
        expect(() => PointFactory.create2DPoint(-3, -2)).not.toThrow();
    });

    test('should not throw an error if coordinates are negative for 3D point', () => {
        expect(() => PointFactory.create3DPoint(-3, -2, -5)).not.toThrow();
    });
});
