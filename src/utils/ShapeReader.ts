import fs from 'fs';
import { logger } from './Logger';

export function readShapesFromFile(filePath: string): string[] {
    logger.info(`Reading shapes from file: ${filePath}`);
    const content = fs.readFileSync(filePath, 'utf-8');
    const lines = content.split('\n');

    return lines;
}
