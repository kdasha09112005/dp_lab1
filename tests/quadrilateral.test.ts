import { QuadrilateralFactory } from '../src/factories/QuadrilateralFactory';
import { PointFactory } from '../src/factories/PointFactory';
import { describe, test, expect, beforeEach } from "@jest/globals"

describe('Quadrilateral 1', () => {
    let quadrilateral: ReturnType<typeof QuadrilateralFactory.createQuadrilateral>;

    beforeEach(() => {
        const p1 = PointFactory.create2DPoint(0, 0);
        const p2 = PointFactory.create2DPoint(4, 0);
        const p3 = PointFactory.create2DPoint(4, 3);
        const p4 = PointFactory.create2DPoint(0, 3);
        quadrilateral = QuadrilateralFactory.createQuadrilateral('test-1', p1, p2, p3, p4);
    });

    test('Area calculation', () => {
        const result = quadrilateral.area();
        expect(result).toBe(12);
    });

    test('Perimeter calculation', () => {
        const result = quadrilateral.perimeter();
        expect(result).toBe(14);
    });

    test('Check if the quadrilateral is a rectangle', () => {
        expect(quadrilateral.isRectangle()).toBe(true);
    });

    test('Check if the quadrilateral is convex', () => {
        expect(quadrilateral.isConvex()).toBe(true);
    });

    test('Check if the quadrilateral is a rhombus', () => {
        expect(quadrilateral.isRhombus()).toBe(false);
    });

    test('Check if the quadrilateral is a trapezoid', () => {
        expect(quadrilateral.isTrapezoid()).toBe(false);
    });
});

describe('Quadrilateral 2', () => {
    let quadrilateral: ReturnType<typeof QuadrilateralFactory.createQuadrilateral>;

    beforeEach(() => {
        const p1 = PointFactory.create2DPoint(1, 1);
        const p2 = PointFactory.create2DPoint(6, 1);
        const p3 = PointFactory.create2DPoint(6, 5);
        const p4 = PointFactory.create2DPoint(1, 5);
        quadrilateral = QuadrilateralFactory.createQuadrilateral('test-2', p1, p2, p3, p4);
    });

    test('Area calculation', () => {
        const result = quadrilateral.area();
        expect(result).toBe(20);
    });

    test('Perimeter calculation', () => {
        const result = quadrilateral.perimeter();
        expect(result).toBe(18);
    });

    test('Check if the quadrilateral is a rectangle', () => {
        expect(quadrilateral.isRectangle()).toBe(true);
    });

    test('Check if the quadrilateral is convex', () => {
        expect(quadrilateral.isConvex()).toBe(true);
    });

    test('Check if the quadrilateral is a rhombus', () => {
        expect(quadrilateral.isRhombus()).toBe(false);
    });

    test('Check if the quadrilateral is a trapezoid', () => {
        expect(quadrilateral.isTrapezoid()).toBe(false);
    });
});

describe('Quadrilateral 2', () => {
    let quadrilateral: ReturnType<typeof QuadrilateralFactory.createQuadrilateral>;

    beforeEach(() => {
        const p1 = PointFactory.create2DPoint(1, 1);
        const p2 = PointFactory.create2DPoint(6, 1);
        const p3 = PointFactory.create2DPoint(6, 5);
        const p4 = PointFactory.create2DPoint(1, 5);
        quadrilateral = QuadrilateralFactory.createQuadrilateral('test-2', p1, p2, p3, p4);
    });

    test('Area calculation', () => {
        const result = quadrilateral.area();
        expect(result).toBe(20);
    });

    test('Perimeter calculation', () => {
        const result = quadrilateral.perimeter();
        expect(result).toBe(18);
    });

    test('Check if the quadrilateral is a rectangle', () => {
        expect(quadrilateral.isRectangle()).toBe(true);
    });

    test('Check if the quadrilateral is convex', () => {
        expect(quadrilateral.isConvex()).toBe(true);
    });

    test('Check if the quadrilateral is a rhombus', () => {
        expect(quadrilateral.isRhombus()).toBe(false);
    });

    test('Check if the quadrilateral is a trapezoid', () => {
        expect(quadrilateral.isTrapezoid()).toBe(false);
    });
});

describe('Quadrilateral Invalid Data', () => {
    
    test('Throws error when three points are collinear', () => {
        const p1 = PointFactory.create2DPoint(0, 0);
        const p2 = PointFactory.create2DPoint(2, 2);
        const p3 = PointFactory.create2DPoint(4, 4);
        const p4 = PointFactory.create2DPoint(1, 3);

        expect(() => {
            QuadrilateralFactory.createQuadrilateral('invalid-1', p1, p2, p3, p4);
        }).toThrow();
    });

    test('Throws error when four points are collinear', () => {
        const p1 = PointFactory.create2DPoint(0, 0);
        const p2 = PointFactory.create2DPoint(2, 2);
        const p3 = PointFactory.create2DPoint(4, 4);
        const p4 = PointFactory.create2DPoint(6, 6);

        expect(() => {
            QuadrilateralFactory.createQuadrilateral('invalid-2', p1, p2, p3, p4);
        }).toThrow();
    });

    test('Throws error when not enough points provided', () => {
        const p1 = PointFactory.create2DPoint(0, 0);
        const p2 = PointFactory.create2DPoint(1, 1);
        const p3 = PointFactory.create2DPoint(2, 2);

        expect(() => {
            QuadrilateralFactory.createQuadrilateral('invalid-6', p1, p2, p3, null as any);
        }).toThrow();
    });
});