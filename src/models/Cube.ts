import { Point } from './Point';

export class Cube {
    private id: string;

    constructor(public origin: Point, public edgeLength: number, idSuffix: string) {
        this.id = this.formatName(idSuffix);
    }

    private formatName(suffix: string): string {
        return `Cube-${suffix}`;
    }

    surfaceArea(): number {
        return 6 * (this.edgeLength ** 2);
    }

    volume(): number {
        return this.edgeLength ** 3;
    }

    isCube(): boolean {
        return true;
    }

    baseOnCoordinatePlane(): boolean {
        return this.origin.z === 0;
    }

    volumeRatioAfterSliceXY(): number {
        const midZ = this.origin.z + this.edgeLength / 2;
        if (midZ <= this.origin.z || midZ >= this.origin.z + this.edgeLength) {
            return 1;
        }
        return 1;
    }

    getId(): string {
        return this.id;
    }
}
