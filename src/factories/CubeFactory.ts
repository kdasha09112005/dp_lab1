import { Cube } from '../models/Cube';
import { Point } from '../models/Point';
import { DataValidator } from '../validators/DataValidator';

export class CubeFactory {
    static createCube(p1: Point, edgeLength: number, idSuffix: string): Cube {
        if (edgeLength <= 0 || DataValidator.isEmpty(edgeLength)) {
            throw new Error('Edge length must be positive.');
        }

        return new Cube(p1, edgeLength, idSuffix);
    }
}
