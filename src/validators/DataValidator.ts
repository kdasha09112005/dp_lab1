import { logger } from '../utils/Logger';

const QUAD_DATA: string = "Invalid quadrilateral data:";
const CUBE_DATA: string = "Invalid cube data:";

export class DataValidator {
    static isValidPointData(parts: string[]): boolean {
        if (parts.length !== 8) {
            logger.error(`${QUAD_DATA} There should be exactly 8 numbers for four points of a quadrilateral.`);
            return false;
        }
        const allNumbers = parts.every(part => !isNaN(parseFloat(part)));
        if (!allNumbers) {
            logger.error(`${QUAD_DATA} All parts must be valid numbers.`);
            return false;
        }
        return true;
    }

    static canFormQuadrilateral(parts: string[]): boolean {
        if (!this.isValidPointData(parts)) {
            return false;
        }
        return true;
    }

    static isValidCubeData(parts: string[]): boolean {
        if (parts.length !== 4) {
            logger.error(`${CUBE_DATA} There should be exactly 4 numbers for one point and edge length.`);
            return false;
        }
        const allValid = parts.slice(0, 3).every(part => !isNaN(parseFloat(part))) && parseFloat(parts[3]) > 0;
        if (!allValid) {
            logger.error(`${CUBE_DATA} All parts must be valid numbers and edge length must be positive.`);
            return false;
        }
        return true;
    }

    static isEmpty(val: number): boolean {
        return (val === undefined || val == null || Number.isNaN(val)) ? true : false;
    }
}
