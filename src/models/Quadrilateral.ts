import { Point } from './Point';

export class Quadrilateral {
    constructor(public id: string, public p1: Point, public p2: Point, public p3: Point, public p4: Point) {
        this.id = this.formatName(id);
    }

    static collinear(p1: Point, p2: Point, p3: Point): boolean {
        return (p2.y - p1.y) * (p3.x - p2.x) === (p3.y - p2.y) * (p2.x - p1.x);
    }

    private formatName(name: string): string {
        return `Quadrilateral-${name}`;
    }

    private distance(p1: Point, p2: Point): number {
        return Math.sqrt((p2.x - p1.x) ** 2 + (p2.y - p1.y) ** 2);
    }

    private calculateSides() {
        const d1 = this.distance(this.p1, this.p2);
        const d2 = this.distance(this.p2, this.p3);
        const d3 = this.distance(this.p3, this.p4);
        const d4 = this.distance(this.p4, this.p1);

        const lengths = [d1, d2, d3, d4];
        const width = Math.min(...lengths);
        const height = Math.max(...lengths);
        return { d1, d2, d3, d4, width, height };
    }

    private evaluateSlope(p1: Point, p2: Point): number | null {
        if (p2.x - p1.x === 0) {
            return null;
        }
        return (p2.y - p1.y) / (p2.x - p1.x);
    }

    private calculateSlopes() {
        const slope12 = this.evaluateSlope(this.p1, this.p2);
        const slope34 = this.evaluateSlope(this.p3, this.p4);
        const slope23 = this.evaluateSlope(this.p2, this.p3);
        const slope41 = this.evaluateSlope(this.p4, this.p1);

        return { slope12, slope34, slope23, slope41};
    }

    private areAllSidesEqual(): boolean {
        const { d1, d2, d3, d4 } = this.calculateSides();
        return d1 === d2 && d2 === d3 && d3 === d4;
    }

    private areOppositeSidesEqual(): boolean {
        const { d1, d2, d3, d4 } = this.calculateSides();
        return (d1 === d3 && d2 === d4);
    }

    private areOppositeSidesParallel(): boolean {
        const { slope12, slope34, slope23, slope41 } = this.calculateSlopes();
        return (slope12 === slope34 && slope23 === slope41);
    }

    private areAdjacentSidesPerpendicular(): boolean {
        const { slope12, slope23, slope34, slope41 } = this.calculateSlopes();
        return (
            (slope12 === null && slope23 === 0) ||
            (slope12 === 0 && slope23 === null) ||
            (slope23 === null && slope34 === 0) ||
            (slope23 === 0 && slope34 === null) ||
            (slope34 === null && slope41 === 0) ||
            (slope34 === 0 && slope41 === null) ||
            (slope41 === null && slope12 === 0) ||
            (slope41 === 0 && slope12 === null)
        );
    }

    isRectangle(): boolean {
        return this.areOppositeSidesEqual() && this.areOppositeSidesParallel() && this.areAdjacentSidesPerpendicular() && !this.isSquare();
    }

    isSquare(): boolean {
        return this.areAllSidesEqual() && this.areOppositeSidesParallel() && this.areAdjacentSidesPerpendicular();
    }

    isTrapezoid(): boolean {
        const { slope12, slope34, slope23, slope41 } = this.calculateSlopes();

        const parallelPairs = [
            (slope12 === slope34),
            (slope23 === slope41)
        ];

        return parallelPairs.filter(isParallel => isParallel).length === 1;
    }

    isRhombus(): boolean {
        return this.areAllSidesEqual() && this.areOppositeSidesParallel();
    }

    perimeter(): number {
        const { d1, d2, d3, d4 } = this.calculateSides();
        return Math.round(d1 + d2 + d3 + d4);
    }

    area(): number {
        if (this.isRectangle()) {
            const { width, height } = this.calculateSides();
            return width * height;
        } else {
            return Math.abs(
                this.p1.x * this.p2.y + this.p2.x * this.p3.y + this.p3.x * this.p4.y + this.p4.x * this.p1.y -
                this.p1.y * this.p2.x - this.p2.y * this.p3.x - this.p3.y * this.p4.x - this.p4.y * this.p1.x
            ) / 2;
        }
    }

    isConvex(): boolean {
        const crossProduct = (p1: Point, p2: Point, p3: Point) => 
            (p2.x - p1.x) * (p3.y - p2.y) - (p2.y - p1.y) * (p3.x - p2.x);

        const z1 = crossProduct(this.p1, this.p2, this.p3);
        const z2 = crossProduct(this.p2, this.p3, this.p4);
        const z3 = crossProduct(this.p3, this.p4, this.p1);
        const z4 = crossProduct(this.p4, this.p1, this.p2);

        return (z1 > 0 && z2 > 0 && z3 > 0 && z4 > 0) || (z1 < 0 && z2 < 0 && z3 < 0 && z4 < 0);
    }
}
