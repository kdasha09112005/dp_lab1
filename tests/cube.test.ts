import { CubeFactory } from '../src/factories/CubeFactory';
import { PointFactory } from '../src/factories/PointFactory';
import { describe, test, expect, beforeEach } from "@jest/globals"

describe('Cube', () => {
    let cube: ReturnType<typeof CubeFactory.createCube>;

    beforeEach(() => {
        const origin = PointFactory.create3DPoint(0, 0, 0);
        const edgeLength = 3;
        const idSuffix = 'test-cube1';
        cube = CubeFactory.createCube(origin, edgeLength, idSuffix);
    });

    test('Surface area calculation', () => {
        const expectedSurfaceArea = 6 * (3 ** 2);
        expect(cube.surfaceArea()).toBe(expectedSurfaceArea);
    });

    test('Volume calculation', () => {
        const expectedVolume = 3 ** 3;
        expect(cube.volume()).toBe(expectedVolume);
    });

    test('Check if cube base is on XY plane', () => {
        expect(cube.baseOnCoordinatePlane()).toBe(true);
    });

    test('Check if object is cube', () => {
        expect(cube.isCube()).toBe(true);
    });

    test('Volume ratio after slicing XY', () => {
        expect(cube.volumeRatioAfterSliceXY()).toBe(1);
    });
});

describe('Cube with origin above XY plane', () => {
    let cube: ReturnType<typeof CubeFactory.createCube>;

    beforeEach(() => {
        const origin = PointFactory.create3DPoint(0, 0, 5);
        const edgeLength = 2;
        const idSuffix = 'test-cube2';
        cube = CubeFactory.createCube(origin, edgeLength, idSuffix);
    });

    test('Surface area calculation', () => {
        const expectedSurfaceArea = 6 * (2 ** 2);
        expect(cube.surfaceArea()).toBe(expectedSurfaceArea);
    });

    test('Volume calculation', () => {
        const expectedVolume = 2 ** 3;
        expect(cube.volume()).toBe(expectedVolume);
    });

    test('Check if cube base is on XY plane', () => {
        expect(cube.baseOnCoordinatePlane()).toBe(false);
    });

    test('Volume ratio after slicing XY', () => {
        expect(cube.volumeRatioAfterSliceXY()).toBe(1);
    });
});

describe('Checking data for errors', () => {
    test('Negative edge length should throw error', () => {
        const origin = PointFactory.create3DPoint(0, 0, 0);
        expect(() => CubeFactory.createCube(origin, -1, 'invalid-cube-01')).toThrow();
    });

    test('Zero edge length should throw error', () => {
        const origin = PointFactory.create3DPoint(10, 10, 10);
        expect(() => CubeFactory.createCube(origin, 0, 'invalid-cube-02')).toThrow();
    });

    test('Non-numeric edge length should throw error', () => {
        const origin = PointFactory.create3DPoint(5, 5, 5);
        expect(() => CubeFactory.createCube(origin, NaN, 'invalid-cube-03')).toThrow();
    });

    test('Non-integer edge length should not throw error', () => {
        const origin = PointFactory.create3DPoint(6, 6, 6);
        expect(() => CubeFactory.createCube(origin, 2.5, 'valid-cube-01')).not.toThrow();
    });
});
